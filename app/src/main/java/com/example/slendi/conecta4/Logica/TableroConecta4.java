package com.example.slendi.conecta4.Logica;

import java.util.ArrayList;
import java.util.prefs.Preferences;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Movimiento;

/**
 * Created by slendi on 30/01/16.
 */
public class TableroConecta4 extends es.uam.eps.multij.Tablero {

    int [][] tablero;
    int TamFila = 6;
    int TamColumna = 7;

    /**
     * Constructor
     */

    public TableroConecta4 (){
        super();
        this.estado = EN_CURSO;
        this.turno = 0;
        this.ultimoMovimiento = null;
        iniTablero();
    }

    /**
     * Metodo que crea el tablero inicial del juego, con 0 en las casillas.
     */
    public void iniTablero(){
        this.tablero = new int[this.TamFila][this.TamColumna];
        for(int i = 0; i<this.TamFila ; i++){
            for(int j = 0; j<this.TamColumna ; j++){
                this.tablero[i][j] = 0;
            }
        }
    }

    /**
     * Metodo que realiza el movimiento en el tablero
     * @param movimiento movimiento que se va a realizar
     * @throws ExcepcionJuego
     */
    @Override
    public void mueve(Movimiento movimiento) throws ExcepcionJuego {
        int col = ((MovimientoConecta4)movimiento).columna;
        if(esValido(movimiento)){
            this.ultimoMovimiento = movimiento;
            for(int i = 0; i<this.TamFila ; i++){
                if(this.tablero[i][col] == 0){
                    this.tablero[i][col] =  this.turno+1;
                    break;
                }
            }
            compPartida();
        }
    }

    /**
     * Metodo que comprueba si el movimiento es valido.
     * @param movimiento movimiento que se va a comprobar.
     * @return true si se puede.
     * @return false si no.
     */
    @Override
    public boolean esValido(Movimiento movimiento) {
        return movimientosValidos().contains(new MovimientoConecta4(((MovimientoConecta4)movimiento).columna));
    }

    /**
     * Metodo que devuelve los movimientos validos.
     * @return Validos ArrayList de movimientos validos.
     */
    @Override
    public ArrayList<Movimiento> movimientosValidos() {
        ArrayList<Movimiento> Validos = new ArrayList<Movimiento>();
        for (int i = 0; i< this.TamFila ; i++){
            for(int j = 0 ; j<this.TamColumna ; j++){
                if( (this.tablero[i][j]==0) && !Validos.contains(new MovimientoConecta4(j))){
                    Validos.add(new MovimientoConecta4(j));
                }
            }
        }
        //System.out.println(Validos.toString());
        return Validos;
    }

    /**
     * Metodo que pone el tablero en forma de string.
     * @return Tab tablero en formato string.
     */
    @Override
    public String tableroToString() {
        String Tab = "";
        for (int i = (this.TamFila-1); i >= 0 ; i--) {
            for (int j = 0; j < this.TamColumna; j++) {
                Tab += this.tablero[i][j] + "|";
            }
            Tab = Tab.substring(0,Tab.length()-1);
            Tab += "\n";
            //Tab += "-";
        }
        return Tab;
    }

    /**
     * Metodo que reconvierte el tablero.
     * @param s cadena a convertir
     * @throws ExcepcionJuego
     */
    @Override
    public void stringToTablero(String s) throws ExcepcionJuego {
        String [] filas = s.split("-");
        int f = 0;
        for (String val : filas) {
            for (int j = 0; j < val.length(); j++) {
                this.tablero[this.TamFila-f-1][j] = Integer.parseInt(val.charAt(j)+"");
            }
            f++;
        }

    }

    /**
     * Metodo que comprueba si hay final de partida.
     */
    public void compPartida(){
        if ( compFilas() || compColumnas() || compDiagonales() )
            this.estado = 2; //TERMINADo
        else if(movimientosValidos().size() == 0)
            this.estado = 3; //TABLAS
         else
            cambiaTurno();
    }

    /**
     * Metodo que comprueba filas.
     * @return true si esta correcto.
     * @return false si hay final.
     */
    private boolean compFilas() {
        for (int i = 0; i< this.TamFila ; i++) {
            for (int j = 0; (j + 3) < this.TamColumna; j++) {
                if( this.tablero[i][j] == this.tablero[i][j+1] &&
                        this.tablero[i][j] == this.tablero[i][j+2] &&
                        this.tablero[i][j] == this.tablero[i][j+3] &&
                        this.tablero[i][j] != 0)
                    return  true;
            }
        }
        return false;
    }


    /**
     * Metodo que comprueba columnas.
     * @return true si esta correcto.
     * @return false si hay final.
     */
    private boolean compColumnas() {
        for (int j = 0; j< this.TamColumna ; j++) {
            for (int i = 0; (i + 3) < this.TamFila; i++) {
                if( this.tablero[i][j] == this.tablero[i+1][j] &&
                        this.tablero[i][j] == this.tablero[i+2][j] &&
                        this.tablero[i][j] == this.tablero[i+3][j] &&
                        this.tablero[i][j] != 0)
                    return  true;
            }
        }
        return false;
    }


    /**
     * Metodo que comprueba diagonales.
     * @return true si esta correcto.
     * @return false si hay final.
     */
    private boolean compDiagonales() {
        for (int i = 3; i< this.TamFila ; i++) {
            for (int j = 0; (j + 3) < this.TamColumna; j++) {
                if( (this.tablero[i][j] == this.tablero[i-1][j+1] &&
                        this.tablero[i][j] == this.tablero[i-2][j+2] &&
                        this.tablero[i][j] == this.tablero[i-3][j+3] &&
                        this.tablero[i][j] != 0)
                        ||
                        (this.tablero[i-3][j] == this.tablero[i-2][j+1] &&
                                this.tablero[i-3][j] == this.tablero[i-1][j+2] &&
                                this.tablero[i-3][j] == this.tablero[i][j+3] &&
                                this.tablero[i-3][j] != 0) ){
                    return true;
                }

            }
        }
        return false;
    }


    /**
     * Metodo que imprime tablero.
     * @return false si hay final.
     */
    @Override
    public String toString() {
        return tableroToString();
    }


    /**
     * Metodo que devuelve el tablero de la partida.
     * @return tablero de la partida.
     */
    public int[][] getTableroPartida (){
        return this.tablero;
    }


    /**
     * Metodo que devuelve el contenido de una posicion del tablero.
     * @return contenido de la posicion del tablero.
     */
    public int getCeldaTablero(int f, int c){
        return this.tablero[f][c];
    }
}
