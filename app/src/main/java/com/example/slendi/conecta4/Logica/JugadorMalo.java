package com.example.slendi.conecta4.Logica;

import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

/**
 * Created by slendi on 2/02/16.
 */
public class JugadorMalo implements Jugador {
        private String nombre;
        private static int numAleatorios = 0;

        public JugadorMalo() {
            this("Aleatorio " + ++numAleatorios);
        }

        public JugadorMalo(String nombre) {
            this.nombre = nombre;
        }

        public void onCambioEnPartida(Evento evento) {
            switch(evento.getTipo()) {
                case Evento.EVENTO_CAMBIO:
                    System.out.println(nombre + ": Cambio:" + evento.getDescripcion());
                    System.out.println(nombre + ": Tablero es:\n" + evento.getPartida().getTablero());
                    break;
                case 2:
                    System.out.println(nombre + ": Turno:" + evento.getDescripcion());

                    Tablero t = evento.getPartida().getTablero();
                    int r = (int)(Math.random() * (double)t.movimientosValidos().size());

                    try {
                        evento.getPartida().realizaAccion(new AccionMover(this, (MovimientoConecta4)t.movimientosValidos().get(r)));
                    } catch (Exception var5) {
                        ;
                    }
                    break;
                case 3:
                    System.out.println(nombre + ": Confirmacion:" + evento.getDescripcion());

                    try {
                        evento.getPartida().confirmaAccion(this, evento.getCausa(), Math.random() > 0.5D);
                    } catch (Exception var6) {
                        ;
                    }
            }

        }

        public String getNombre() {
            return this.nombre;
        }

        public boolean puedeJugar(Tablero tablero) {
            return true;
        }
    }

