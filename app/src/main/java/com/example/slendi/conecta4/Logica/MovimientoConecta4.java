package com.example.slendi.conecta4.Logica;


/**
 * Created by slendi on 30/01/16.
 */
public class MovimientoConecta4 extends es.uam.eps.multij.Movimiento{

    int columna;

    /**
     * Constructor del movimiento.
     * @param columna que se va a colocar.
     */
    public MovimientoConecta4 (int columna){
        super();
        this.columna = columna;
    }

    /**
     * Metodo que imprime el movimiento que se hara.
     * @return movimiento a realizar.
     */
    @Override
    public String toString() {
        return Integer.toString(this.columna);
    }


    /**
     * Metodo que comprueba igualdad de movimientos.
     * @return true si iguales.
     * @return false si distintos.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof MovimientoConecta4){
            int columnaComp = ((MovimientoConecta4)o).columna;
            if (columnaComp == this.columna)
                return true;
        }
        return false;
    }
}
