package com.example.slendi.conecta4.Logica;

import android.content.SyncStatusObserver;

import java.util.Collections;
import java.util.Scanner;

import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

/**
 * Created by slendi on 30/01/16.
 */
public class JugadorHumano implements Jugador {

    private String nombre;
    private static int aleat;

    public JugadorHumano () {
        this("Aleatorio "+ (++aleat));
    }

    public JugadorHumano(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Devuelve el nombre del jugador
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Este jugador juega *todos* los juegos
     */
    public boolean puedeJugar(Tablero tablero)  {
        return true;
    }

    @Override
    public void onCambioEnPartida(Evento evento) {
        switch (evento.getTipo()) {
            case Evento.EVENTO_CAMBIO:
                System.out.println(nombre + ": Cambio:" + evento.getDescripcion());
                System.out.println(nombre + ": Tablero es:\n" + evento.getPartida().getTablero());
                break;

            case Evento.EVENTO_CONFIRMA:
                // este jugador confirma al azar
                System.out.println(nombre + ": Confirmacion:" + evento.getDescripcion());
                try {
                    evento.getPartida().confirmaAccion(
                            this, evento.getCausa(), (Math.random() > .5));
                }
                catch(Exception e) {

                }
                break;

            case Evento.EVENTO_TURNO:
                System.out.println(nombre + ": Turno:" + evento.getDescripcion());
                System.out.println("INTRODUCE COLUMNA PARA COLOCAR LA FICHA");
                Scanner sc = new Scanner (System.in);
                int col=0;

                Tablero t = evento.getPartida().getTablero();
                col = sc.nextInt();
                while( (col < 0 || col > 6) ){
                    System.out.println("COLUMNA ERRÓNEA!!, INTRODUCE UNA NUEVA.");
                    col = sc.nextInt();
                }
                try {
                    MovimientoConecta4 nm= new MovimientoConecta4(col);
                    while(t.esValido(nm) == false){
                        System.out.println("Movimiento erroneo, haga otro.");
                        col = sc.nextInt();
                        while( (col < 0 || col > 6) ){
                            System.out.println("COLUMNA ERRÓNEA!!, INTRODUCE UNA NUEVA.");
                            col = sc.nextInt();
                        }
                        nm = new MovimientoConecta4(col);
                    }
                    evento.getPartida().realizaAccion(new AccionMover(this, nm));
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
