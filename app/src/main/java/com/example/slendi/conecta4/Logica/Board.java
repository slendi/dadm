package com.example.slendi.conecta4.Logica;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import Principal.MainActivity;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;

/**
 * Created by slendi on 29/02/16.
 */
public class Board extends View implements Jugador {

    private final int TAMCOL = 7;
    private final int TAMFIL = 6;

    private float altura;
    private float anchura;

    private MovimientoConecta4 mov;

    private Paint circle, back;

    private int anchuraT;
    private int alturaT;

    private Partida partida;

    MainActivity main;

    private float x, y;

    private boolean turno = false;

    private Thread t;

    private boolean numJ = false;

    public Board(Context con, AttributeSet att){
        super(con, att);
        this.main = (MainActivity) con;

        setFocusable(true);
        setFocusableInTouchMode(true);
        circle = new Paint(Paint.ANTI_ALIAS_FLAG);
        circle.setStrokeWidth(2);
        back = new Paint(Paint.ANTI_ALIAS_FLAG);
        back.setColor(Color.GRAY);
        this.x = 0;
        this.y = 0;

    }

    protected void onSizeChanged (int ancho, int alto, int anchoANT, int alturaANT){
        if( ancho < alto){
            alto = ancho;
        } else {
            ancho = alto;
        }

        anchuraT = ancho;
        alturaT = alto;

        this.anchura = ancho / TAMCOL;
        this.altura = alto / TAMFIL;
        super.onSizeChanged(ancho, alto, anchoANT, alturaANT);
    }

    private void drawTile (Canvas c, Paint p){
        int casVacia, j1, j2;
        float x_cent, y_cent, radio;

        radio = getResources().getDimension(R.dimen.circle_radio);
        casVacia = getResources().getColor(R.color.emptyPos);
        j1 = getResources().getColor(R.color.Jugador1);
        j2 = getResources().getColor(R.color.Jugador2);

        for (int i = 0; i<TAMFIL ; i++){
            y_cent = altura * (1 + 2 * (TAMFIL - 1 -i)) / 2f;
            for (int j = 0; j< TAMCOL ; j++){
                if ((((TableroConecta4)this.partida.getTablero()).getCeldaTablero(i,j)) == 0){
                    p.setColor(casVacia);
                } else if ((((TableroConecta4)this.partida.getTablero()).getCeldaTablero(i,j)) == 1){
                    p.setColor(j1);
                } else {
                    p.setColor(j2);
                }
                x_cent = anchura * (1 + 2 * j) / 2f;
                c.drawCircle(this.x + x_cent, this.y + y_cent, radio, p);
            }
        }

    }

    protected void onDraw (Canvas c){
        super.onDraw(c);
        float left, right, top, bot;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            left = (c.getWidth() - anchuraT) / 2;
            this.x = left;
            this.y = 0;
            top = 0;
            right = anchura + left;
            bot = alturaT;
        } else {
            left = 10;
            top = 0;
            this.x = 0;
            this.y = top;
            right = anchuraT - left;
            bot = alturaT;
        }
        c.drawRect(left, top, right, bot, back);
        drawTile(c, circle);
    }

    public boolean onTouchEvent (MotionEvent event){
        if (this.partida.getTablero().getEstado() == Tablero.FINALIZADA || this.partida.getTablero().getEstado() == Tablero.EN_CURSO){
            return false;
        }

        int indiceX, indiceY;

        int accion = event.getAction();
        switch (accion) {
            case MotionEvent.ACTION_DOWN:
                float x = event.getX();
                float y = event.getY();

                indiceX = xToind(x - this.x);
                indiceY = yToind (y - this.y);

                if (indiceX < 0 || indiceY < 0){
                    return false;
                }

                if (! partida.getTablero().esValido(new MovimientoConecta4(indiceX))){
                    return super.onTouchEvent(event);
                }

                try {
                    this.mov = new MovimientoConecta4(indiceX);
                    this.partida.mueve(this, this.mov);
                } catch (ExcepcionJuego e){
                    e.printStackTrace();
                    return false;
                }

                //ZONA IMPORTANTE

                invalidate();

                if (this.partida.getTablero().getEstado() == Tablero.FINALIZADA){
                    Toast.makeText(this.getContext(), R.string.gameOver, Toast.LENGTH_LONG).show();
                    //main.showDialogo();
                } else (this.partida.getTablero().getEstado() == Tablero.TABLAS){
                    Toast.makeText(this.getContext(), R.string.gameOver, Toast.LENGTH_LONG).show();
                    //main.showDialog();
                }
        }
        return super.onTouchEvent(event);
    }

    public Partida getPartida(){
        return partida;
    }

    public void setPartida (Partida p){
        this.partida = p;
    }

    public int xToind(float x){
        int ind = -1;
        if (x < 0){
            return ind;
        }

        if (x < anchura){
            ind = 0;
        } else if (x < 2 * anchura){
            ind = 1;
        } else if (x < 3 * anchura){
            ind = 2;
        } else if (x < 4 * anchura){
            ind = 3;
        } else if (x < 5 * anchura){
            ind = 4;
        } else if (x < 6 * anchura){
            ind = 5;
        } else if (x < 7 * anchura){
            ind = 6;
        }
        return ind;
    }

    private int yToind (float y){
        int ind = -1;
        if (y < 0){
            return ind;
        }

        if (y < anchura){
            ind = 6;
        } else if (y < 2 * altura){
            ind = 5;
        } else if (y < 3 * altura){
            ind = 4;
        } else if (y < 4 * altura){
            ind = 3;
        } else if (y < 5 * altura){
            ind = 2;
        } else if (y < 6 * altura){
            ind = 1;
        } else if (y < 7 * altura){
            ind = 0;
        }
        return ind;
    }

    @Override
    public  void onCambioEnPartida(Evento e){
        switch(e.getTipo()){
            case Evento.EVENTO_TURNO:
                break;
        }
    }

    @Override
    public String getNombre (){
        return "Jugador Humano";
    }

    public boolean isMyTurn(){
        return turno;
    }

    public void setMyTurn (boolean t){
        this.turno = t;
    }

    /*
    @Override
    public boolean onKeyDown(int kc, KeyEvent e){
        if (kc == KeyEvent.KEYCODE_BACK){
            this.t.interrupt();
            this.t = null;
            jugadorSalePartida();
            return super.onKeyDown(kc, e);
        }
        jugadorSalePartida();
        return super.onKeyDown(kc, e);
    }
    */

}
