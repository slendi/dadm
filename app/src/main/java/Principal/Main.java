package Principal;

import com.example.slendi.conecta4.Logica.JugadorMalo;
import com.example.slendi.conecta4.Logica.TableroConecta4;
import com.example.slendi.conecta4.Logica.JugadorHumano;

import java.util.ArrayList;

import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.JugadorAleatorio;
import es.uam.eps.multij.Partida;

/**
 * Created by slendi on 30/01/16.
 */
public class Main {
    public static void main (String[] args){
        JugadorAleatorio ja= new JugadorAleatorio("Maquina1");
        JugadorHumano jh = new JugadorHumano("Humano1");
        JugadorHumano jh2 = new JugadorHumano("Humano2");
        JugadorAleatorio ja2= new JugadorAleatorio("Maquina2");
        ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
        System.out.println("EMPIEZA LA PARTIDA");

        /*PARTIDA MAQUINA VS MAQUINA*/
       // jugadores.add(ja);
        //jugadores.add(ja2);
        /*PARTIDA MAQUINA VS PERSONA HUMANA*/
          jugadores.add(jh);
          jugadores.add(ja);
        /*PARTIDA PERSONA HUMANA VS PERSONA HUMANA*/
        //jugadores.add(jh);
        //jugadores.add(jh2);

        Partida partida = new Partida(new TableroConecta4(), jugadores);
        partida.addObservador(new JugadorHumano("OBS"));
        partida.comenzar();
        System.out.print("FIN DE LA PARTIDA");
    }
}
