package Principal;

import android.app.Activity;
import android.os.Bundle;

import com.example.slendi.conecta4.Logica.Board;
import com.example.slendi.conecta4.Logica.TableroConecta4;
import com.example.slendi.conecta4.R;

import java.util.ArrayList;

import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.JugadorAleatorio;
import es.uam.eps.multij.Partida;

/**
 * Created by slendi on 29/02/16.
 */
public class MainActivity extends Activity{

    private Partida partida;

    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        ArrayList<Jugador> jugadores = new ArrayList<>();
        setContentView(R.layout.activity_main);

        JugadorAleatorio ja = new JugadorAleatorio("M1");
        Board jh = (Board) findViewById(R.id.board);
        jugadores.add(ja);
        jugadores.add(jh);

        partida = new Partida(new TableroConecta4(), jugadores);
        jh.setPartida(partida);
        partida.comenzar();

    }

    public void play(){
        ArrayList<Jugador> jugadores = new ArrayList<>();
        setContentView(R.layout.activity_main);

        JugadorAleatorio ja = new JugadorAleatorio("M1");
        Board jh = (Board) findViewById(R.id.board);
        jugadores.add(ja);
        jugadores.add(jh);

        partida = new Partida(new TableroConecta4(), jugadores);
        jh.setPartida(partida);
        partida.comenzar();
    }
}
